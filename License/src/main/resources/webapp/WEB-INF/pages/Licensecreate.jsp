
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create License</title>
</head>
<body>
<script>  
function validateform(){  
var issuer=document.create.issuer.value;  
var version=document.create.version.value; 
var holder=document.create.holder.value;  
var gadate=document.create.gadate.value; 
var gbdate=document.create.gbdate.value;  
var mail=document.create.mail.value; 
var username=document.create.username.value;  
var pwd=document.create.pwd.value; 
var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  

if (issuer==null || issuer=="" &&version==null || version=="" &&holder==null || holder=="" &&gadate==null || gadate=="" &&gbdate==null || gbdate=="" &&mail==null || mail=="" &&username==null || username=="" &&pwd==null || pwd==""  ){  
  alert("can't be blank");  
  return false;  
}else if(gadate=="yyyy/MM/dd" &&gbdate=="yyyy/MM/dd" )
	{
	alert("You have entered wrong date.Please Entered Correctly");
	return false; 
	
	}

if(mail.match(mailformat))  
{   
return true;  
}  
else  
{  
alert("You have entered an invalid email address!");  
  
return false;  
}  


var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;  
if(pwd.match(passw))   
{   
alert('Correct, try another...')  
return true;  
}  
else  
{   
alert('Wrong...!')  
return false;  
} 
}  
</script>  
 <form action="creates" method="post" name="create" onsubmit="return validateform()">
	<center>
	  <h1	style="color: #2F4F4F; font-size: 125%; font-family: serif; font-weight: 700; margin-top: 1%;">Product Create</h1>
      <table border="1" style="border: 1px thick;" >
	
	 <tr><th>Issuer             		  :		<input type="text" name="issuer" value="mobile_cripto"></th></tr>
	 <tr><th>MAC Address        		  :*	<input type="text" name="version" value="0000000"></th></tr>
	 <tr><th>Holder             		  :*	<input type="text" name="holder"></th></tr>
	 <tr><th>Good After Date              :*	<input type="text" name="goodAfterDate" value="yyyy/MM/dd"></th></tr>
	 <tr><th>Good Before Date             :*	<input type="text" name="goodBeforeDate" value="yyyy/MM/dd"></th></tr>
	 <tr><th>Email                        :*	<input type="text" name="email" value=""></th></tr>
	 <tr><th>User name                    :*	<input type="text" name="username" value=""></th></tr>
	 <tr><th>Password                     :*	<input type="password" name="password" value=""></th></tr>
	
	<c:forEach var="product" items="${product}">
	<tr><th><c:out value="${product.name}"></c:out>  : <input type="text" name="${product.name}" value=""></tr>
	</c:forEach> 

	</table>
<h1 style="color: rgb(64, 224, 208); margin-top: 1%;">
	 <input type="submit" name="action" value="License Create">
</h1>
 <h1 style="color: rgb(64, 224, 208); margin-top: 1%;">

</h1>

</center>

  		<a href="home"	style="color: #ffo; font-size: 100%; font-family: serif; font-weight: 700; margin-left: 60%;">Home</a>


</form>


</body>
</html>