<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>License List</title>
</head>
<body>
<script>
				function ConfirmDelete() 
				{
					var x = confirm("Are you sure you want to delete?");
					if (x)
					return true;
					else
					return false;
				}
				
				function ConfirmUpdate() 
				{
					var x = confirm("Are you sure  want to update?");
					if (x)
					return true;
					else
					return false;
				}
		</script>
	<center>
<h1	style="color: #2F4F4F; font-size: 130%; font-family: serif; font-weight: 700; margin-top: 2%;">License List</h1>
	<table border="1" style="border: 2px scroll;">	
		 <tr>
			<th><font size="3%" color="blue   ">Id</font></th>
		   <th><font size="3%" color="blue   ">Version</font></th>
		   <th><font size="3%" color="blue   ">Holder</font></th>
		   <th><font size="3%" color="blue   ">Issuer</font></th>
		   <th><font size="3%" color="blue   ">Issue Date</font></th>
		   <th><font size="3%" color="blue   ">Good After date</font></th>
	       <th><font size="3%" color="blue   ">Good Before date</font></th>
		   <th><font size="3%" color="blue   ">No of Licenses</font></th>
		   <th><font size="3%" color="blue   ">Delete</font></th>
		   <th><font size="3%" color="blue   ">Update</font></th>
        </tr>
			<c:forEach var="license" items="${licenseList}">
		 <tr>
		  <th><c:out value="${license.id}"></c:out></th>
		   <th><c:out value="${license.version}"></c:out></th>
		   <th><c:out value="${license.holder}"></c:out></th>
		   <th><c:out value="${license.issuer}"></c:out></th>
		   <th><c:out value="${license.issueDate}"></c:out></th>
	       <th><c:out value="${license.goodAfterDate}"></c:out></th>
		   <th><c:out value="${license.goodBeforeDate}"></c:out></th>
	 	   <th><c:out value="${license.noOfLicenses}"></c:out></th>
		
		 <td><a  onclick="return ConfirmDelete();" href="<c:url value='/ldelete/${license.id}' />" >Remove</a></td> 
 		 <td><a   onclick="return ConfirmUpdate();"href="<c:url value='/lupdate/${license.id}' />" >Edit</a></td> 
 		 <td><a href="/download">Click here to download file</a></td>
							
		</tr>
	</c:forEach>
		</table>
	</center>
	<h1 style="color: rgb(64, 224, 208); margin-top: 1%;">
			<a href="home"	style="color: #ffo; font-size: 50%; font-family: serif; font-weight: 700; margin-left: 45%;">Home</a>
			</h1>
</body>
</html>