

<script type="text/javascript">
	$(function() {

		/*$("#startDate").datepicker({
			changeMonth : true,
			changeYear : true,
			onSelect : function(selected) {
				$("#endDate").datepicker("option", "minDate", selected)
			}
		});
		$("#endDate").datepicker({
			changeMonth : true,
			changeYear : true,
			onSelect : function(selected) {
				$("#startDate").datepicker("option", "maxDate", selected)
			}
		});*/

		$("#addlicense").click(function() {

			var date_ini = new Date($('#startDate').val()).getTime();
			var date_end = new Date($('#endDate').val()).getTime();

			if (!$('#device').val()) {
				alert('IMEI cannot be Empty ');
			}
			/*else if (isNaN(date_ini)) {
				alert('Start date cannot be empty');
			} else if (isNaN(date_end)) {
				alert('End date cannot be empty');
			}*/else {
				$('#licenseform').submit();
			}

		});

	});
</script>


<body id="jive-body">
	<div id="jive-main">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tbody>
				<tr valign="top">
					<td width="1%">
						<div id="jive-sidebar-container">
							<div id="jive-sidebar-box">
								<div id="jive-sidebar">
									<ul>
										<li class="currentlink"><a href="#"
											title="Click to see a list of users in the system"
											onmouseover="self.status='Click to see a list of users in the system';return true;"
											onmouseout="self.status='';return true;"> Users Summary</a></li>
											
										<li class=""><a href="/license/license/lic_list"
											title="Click to upload bulk license to the system"
											onmouseover="self.status='Click to list license to the system';return true;"
											onmouseout="self.status='';return true;">List Users</a></li>
											
										<li class=""><a href="/license/license/bulk_lic_upload"
											title="Click to upload bulk license to the system"
											onmouseover="self.status='Click to upload bulk license to the system';return true;"
											onmouseout="self.status='';return true;">Bulk Upload</a></li>
									</ul>
									<br> <img
										src="<%=request.getContextPath()%>/resources/images/blank.gif"
										width="150" height="1" border="0" alt="">
								</div>
							</div>
						</div>
					</td>
					<td width="99%" id="jive-content">
						<h1>  Add/Edit User</h1>
						<div align="center" id="msg">${notification}</div>
						<div id="jive-main-content">
							<form:form modelAttribute="license" id="licenseform"
								commandName="license" method="post">
								<form:hidden path="id" />
								<div>
									<table>
									
									    <tr>
											<td align="right">Name * &nbsp;</td>
											<td><form:input path="name" id="name" cssClass="text" /></td>
										</tr>
										<tr>
											<!-- <td align="right">BB PIN * &nbsp;</td> -->
											<td align="right">IMEI * &nbsp;</td> 
											<td><form:input path="device" id="device"
													cssClass="text" /></td>
										</tr>
										 <tr>
											<td align="right">Email  &nbsp;</td>
											<td><form:input path="email" id="email" cssClass="text" /></td>
										</tr>
										<tr />
										<tr />

										<tr>
											<td align="right">Product &nbsp;</td>
											<td><form:select path="product">
													<form:options items="${products}" itemLabel="name"
														itemValue="id" />
												</form:select></td>
										</tr>
										<tr />
										<tr>
											<td align="right">Start Date &nbsp;</td>

											<td><form:input path="startDate" id="startDate"
													readonly="true" cssClass="text" /></td>
										</tr>
										<tr />
										<tr>
											<td align="right">End Date &nbsp;</td>

											<td><form:input path="endDate" readonly="true"
													id="endDate" cssClass="text" /></td>
										</tr>
										<tr />
										<tr />

										<tr />
										<tr>
											<td align="right">Enable &nbsp;</td>
											<td><form:checkbox path="enable" /></td>

										</tr>

										<tr />
										<tr>
											<%-- 	<td align="right">is Trial &nbsp;</td>
					<td><form:checkbox path="trial" /></td>

				</tr> --%>
										<tr />
										<tr>
											<td>&nbsp;</td>
											<td>
												<table>

													<tr>
														<td><input type="button" id="addlicense" value="Save" /></td>
														<td><input type="button" value="Cancel"
															onClick="javascript: history.go(-1)"></td>
													</tr>
												</table>

											</td>
										</tr>

									</table>
								</div>
							</form:form>

						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- END main -->
