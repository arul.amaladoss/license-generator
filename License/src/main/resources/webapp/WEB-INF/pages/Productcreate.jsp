
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Product </title>
</head>
<body>
<script>  
	function validateform()
	{  
		var name=document.create.name.value;  
		var platform=document.create.platform.value; 
		var description=document.create.description.value;  

		if (name==null || name=="" &&platform==null || platform=="" &&description==null || description=="")
		{  
  			alert("can't be blank");  
  			return false;  
		}
	}  
</script> 
 <form action="create" method="post" name="create" onsubmit="return validateform()">
      <center>
	  <h1	style="color: #2F4F4F; font-size: 125%; font-family: serif; font-weight: 700; margin-top: 10%;">Product Create</h1>
      <table border="1" style="border: 1px thick;" >
	           	 <tr><th>Name            :<input type="text" name="name"></th></tr>
	           	 <tr><th>Platform      :<input type="text" name="platform"></th></tr>
               	<tr><th>Description  :<input type="text" name="description"></th></tr>                
       </table>
       <h1 style="color: rgb(64, 224, 208); margin-top: 1%;">
                 <input type="submit" name="action" value="Product create">
       </h1>
  		<a href="home"	style="color: #ffo; font-size: 100%; font-family: serif; font-weight: 700; margin-left: 30%;">Home</a>
       </center>
</form>
</body>
</html>