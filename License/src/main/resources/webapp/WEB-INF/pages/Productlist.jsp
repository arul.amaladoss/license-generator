<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List Product</title>
</head>
<body>
		<script>
				function ConfirmDelete() 
				{
					var x = confirm("Are you sure you want to delete?");
					if (x)
					return true;
					else
					return false;
				}
				
				function ConfirmUpdate() 
				{
					var x = confirm("Are you sure  want to update?");
					if (x)
					return true;
					else
					return false;
				}
		</script>
	<center>
			<h1	style="color: #2F4F4F; font-size: 130%; font-family: serif; font-weight: 700; margin-top: 2%;">Product List</h1>
					<table border="1" style="border: 2px scroll;">
							<tr>
								<th><font size="3%" color="blue">Id</font></th>
								<th><font size="3%" color="blue">Name</font></th>
								<th><font size="3%" color="blue">Platform</font></th>
								<th><font size="3%" color="blue">Description</font></th>
								<th><font size="3%" color="blue">Delete</font></th>
								<th><font size="3%" color="blue">Update</font></th>
							</tr>
							<c:forEach var="product" items="${productList}">
								<tr>
									<td><c:out value="${product.id}"></c:out></td>
									<td><c:out value="${product.name}"></c:out></td>
									<td><c:out value="${product.platform}"></c:out></td>
									<td><c:out value="${product.description}"></c:out></td>

									 <td><a  onclick="return ConfirmDelete();" href="<c:url value='/delete/${product.id}' />" >Remove</a></td> 
 									<td><a   onclick="return ConfirmUpdate();"href="<c:url value='/update/${product.id}' />" >Edit</a></td> 
							</tr>
							</c:forEach>
						</table>
</center>
			<h1 style="color: rgb(64, 224, 208); margin-top: 1%;">
			<a href="home"	style="color: #ffo; font-size: 50%; font-family: serif; font-weight: 700; margin-left: 45%;">Home</a>
			</h1>
</body>
</html>