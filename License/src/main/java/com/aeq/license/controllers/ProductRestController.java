/*package com.aeq.license.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.aeq.license.dao.ProductDao;
import com.aeq.license.model.Product;

@RestController
public class ProductRestController 
{
	
	
	//Repository AutoWired
	@Autowired
	private ProductDao productDao;

	
	//--------------------retrive All Product------------------
	
	@RequestMapping(value = "/product/", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> viewAll() 
	{
		List<Product> products=productDao.findAll();
		if(products.isEmpty())
		{
			return  new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Product>>(products,HttpStatus.OK);
	}
	
	
	//---------------------------retrive Single Product----------------------
	
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE 	)
	public ResponseEntity<Product> view(@PathVariable Long id)
	{
		Product product=productDao.findOne(id);
		if(product==null)
		{
			return  new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
			
		}

			return  new ResponseEntity<Product>(HttpStatus.OK);


	}
	
	
	//---------------------------Create a product----------------------------
	
	@RequestMapping(value = "/product/", method = RequestMethod.POST)
	public ResponseEntity<Void> add(@RequestBody Product product, UriComponentsBuilder ucBuilder) 
	{
	
		if(product!=null)
		{
		productDao.save(product);
		}
		
		
		HttpHeaders headers=new HttpHeaders();
		headers.setLocation(ucBuilder.path("/add/{id}").buildAndExpand(product.getId()).toUri());
		 return  new ResponseEntity<Void>(headers,HttpStatus.CREATED);
	}

	
	//-------------------Delete a product-----------------------------

	@RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Product> delete(@PathVariable Long id) 
	{
		Product product=productDao.findById(id);
		
		if (product==null)

		{
			 return  new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
			
			
		} 
		
		productDao.delete(id);
		 return  new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
	}

	//------------------------Update A Product------------------
	
	
	@RequestMapping(value = "/product/", method = RequestMethod.PUT)
	public ResponseEntity<Product> update(@RequestBody Product product) 
	{

		Product products = productDao.findOne(product.getId());
		if(products==null)
		{
			return  new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
		}
		products.setName(product.getName());
		products.setPlatform(product.getPlatform());
		products.setDescription(product.getDescription());
		products = productDao.save(products);

		return  new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
	}
	
	
	
	//------------------------------------------------------------end
	
	
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public Boolean exits(@PathVariable Long id) 
	{
		return productDao.exists(id);
	}
}
*/