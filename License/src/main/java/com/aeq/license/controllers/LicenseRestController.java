/*package com.aeq.license.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.aeq.license.dao.LicenseDao;
import com.aeq.license.model.License;

@RestController
public class LicenseRestController 
{
	

	
	
	//Repository AutoWired
	
	@Autowired
	private LicenseDao licenseDao;
	
	//--------------------retrive All License------------------
	
	@RequestMapping(value = "/viewal", method = RequestMethod.GET)
	public ResponseEntity<List<License>> viewAll() 
	{
		List<License> licenses=licenseDao.findAll();
		if(licenses.isEmpty())
		{
			return  new ResponseEntity<List<License>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<License>>(licenses,HttpStatus.OK);
	}
	
	//---------------------------retrive Single License----------------------
	
	@RequestMapping(value = "/licens/{id}", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE 	)
	public ResponseEntity<License> view(@PathVariable Long id)
	{
		License license=licenseDao.findOne(id);
		if(license==null)
		{
			return  new ResponseEntity<License>(HttpStatus.NOT_FOUND);
			
		}

			return  new ResponseEntity<License>(HttpStatus.OK);


	}
	
	
	//---------------------------Create a License----------------------------
	
	@RequestMapping(value = "/ad", method = RequestMethod.POST)
	public ResponseEntity<Void> add(@RequestBody License license, UriComponentsBuilder ucBuilder) 
	{
		if(license!=null)
		{
			licenseDao.save(license);
		}
		
		
		HttpHeaders headers=new HttpHeaders();
		headers.setLocation(ucBuilder.path("/add/{id}").buildAndExpand(license.getId()).toUri());
		 return  new ResponseEntity<Void>(headers,HttpStatus.CREATED);
	}

	
	//-------------------Delete a License-----------------------------

	@RequestMapping(value = "/delet/{id}", method = RequestMethod.GET)
	public ResponseEntity<License> delete(@PathVariable Long id) 
	{
		License license=licenseDao.findById(id);
		
		if (license==null)

		{
			 return  new ResponseEntity<License>(HttpStatus.NOT_FOUND);
			
			
		} 
		
		licenseDao.delete(id);
		 return  new ResponseEntity<License>(HttpStatus.NO_CONTENT);
	}

	//------------------------Update A License------------------
	
	
	@RequestMapping(value = "/updat", method = RequestMethod.POST)
	public ResponseEntity<License> update(@RequestBody License license) 
	{

		License licenses = licenseDao.findOne(license.getId());
		if(licenses==null)
		{
			return  new ResponseEntity<License>(HttpStatus.NOT_FOUND);
		}
		licenses.setIssuer(license.getIssuer());
		licenses.setHolder(license.getHolder());
		licenses.setGoodAfterDate(license.getGoodAfterDate());
		licenses.setGoodBeforeDate(license.getGoodBeforeDate());
		licenses.setEmail(license.getEmail());
		licenses.setUsername(license.getUsername());
		licenses.setPassword(license.getPassword());
		licenses.setIssueDate(license.getIssueDate());
		
		licenses.setVersion(license.getVersion());
		licenses = licenseDao.save(licenses);

		return  new ResponseEntity<License>(HttpStatus.NO_CONTENT);
	}
	
	
	
	//------------------------------------------------------------end
	
	
	@RequestMapping(value = "/exist/{id}", method = RequestMethod.GET)
	public Boolean exits(@PathVariable Long id) 
	{
		return licenseDao.exists(id);
	}

	
	 * @RequestMapping("/update") public License update(@RequestParam Long
	 * id,@RequestParam String name, @RequestParam String platform,String
	 * description) { License License = LicenseDao.findOne(id);
	 * License.setName(name); License.setPlatform(platform);
	 * License.setDescription(description); License = LicenseDao.save(License);
	 * return License; }
	 

	

	//--------------------------------------------------------------------end



}
*/