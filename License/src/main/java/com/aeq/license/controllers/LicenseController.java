/*package com.aeq.license.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aeq.license.dao.LicenseDao;
import com.aeq.license.model.License;

@RestController("/license")
public class LicenseController {

	@Autowired
	private LicenseDao licenseDao;

	@RequestMapping(value = "/adds", method = RequestMethod.POST)
	public String add(@RequestBody License license) {

		licenseDao.save(license);
		return "success";
	}

	@RequestMapping(value = "/views/{id}", method = RequestMethod.GET)
	public License view(@PathVariable Long id) {

		return licenseDao.findOne(id);

	}

	@RequestMapping(value = "/viewalls", method = RequestMethod.GET)
	public List<License> viewAll() {
		return licenseDao.findAll();
	}

	@RequestMapping(value = "/deletes/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable Long id) {
		String message = "";
		if (licenseDao.exists(id))

		{
			licenseDao.delete(id);
			message = "Successfully deleted";
		} else {
			message = "License dosn't exists";
		}

		return message;
	}

	@RequestMapping(value = "/existss/{id}", method = RequestMethod.GET)
	public Boolean exits(@PathVariable Long id) {
		return licenseDao.exists(id);
	}

	
	 * @RequestMapping("/update") public Product update(@RequestParam Long
	 * id,@RequestParam String name, @RequestParam String platform,String
	 * description) { Product product = productDao.findOne(id);
	 * product.setName(name); product.setPlatform(platform);
	 * product.setDescription(description); product = productDao.save(product);
	 * return product; }
	 

	@RequestMapping(value = "/updates", method = RequestMethod.POST)
	public License update(@RequestBody License license) {
		License licenses = licenseDao.findOne(license.getId());
		licenses.setIssuer(license.getIssuer());
		licenses.setHolder(license.getHolder());
		licenses.setGoodAfterDate(license.getGoodAfterDate());
		licenses.setGoodBeforeDate(license.getGoodBeforeDate());
		licenses.setEmail(license.getEmail());
		licenses.setUsername(license.getUsername());
		licenses.setPassword(license.getPassword());
		licenses.setIssueDate(license.getIssueDate());
		
		licenses.setVersion(license.getVersion());

		licenses = licenseDao.save(licenses);

		return licenses;
	}

}
*/