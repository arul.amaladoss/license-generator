/*package com.aeq.license.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aeq.license.dao.ProductDao;
import com.aeq.license.model.Product;

@RestController
public class ProductController 
{
	
	//-------------------------------------------------------------begin
	
	@Autowired
	private ProductDao productDao;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@RequestBody Product product) 
	{
		
		productDao.save(product);
		return "success";
	}

	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public Product view(@PathVariable Long id)
	{

		return productDao.findOne(id);

	}

	@RequestMapping(value = "/viewall", method = RequestMethod.GET)
	public List<Product> viewAll() 
	{
		return productDao.findAll();
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable Long id) 
	{
		String message = "";
		if (productDao.exists(id))

		{
			productDao.delete(id);
			message = "Successfully deleted";
		} else {
			message = "product dosn't exists";
		}

		return message;
	}

	@RequestMapping(value = "/exists/{id}", method = RequestMethod.GET)
	public Boolean exits(@PathVariable Long id) 
	{
		return productDao.exists(id);
	}

	
	  @RequestMapping("/update") 
	  public Product update(@RequestBody Product product) 
	  {
	  Product currentProduct = productDao.findOne(product.getId());
	  currentProduct.setName(product.getName());
	  currentProduct.setPlatform(product.getPlatform());
	  currentProduct.setDescription(product.getDescription());
	  currentProduct = productDao.save(product);
	  return currentProduct;
	  }
	 

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Product update(@RequestBody Product product) 
	{

		Product products = productDao.findOne(product.getId());
		products.setName(product.getName());
		products.setPlatform(product.getPlatform());
		products.setDescription(product.getDescription());
		products = productDao.save(products);

		return products;
	}

	//--------------------------------------------------------------------end

	@RequestMapping("/")
	public String testDisplay()

	{
		return "hello";
	}

	@RequestMapping("product/user/{accountNumber}")
	public String display(@PathVariable final int accountNumber)

	{
		return "user Account Number" + accountNumber;
	}

	@RequestMapping("product/admin/{accountNumber}")
	public String displayAll(@PathVariable final int accountNumber)

	{
		return "admin Account Number" + accountNumber;
	}

	@RequestMapping("product/admin/admin/{accountNumber}")
	public String extRAdisplayAll(@PathVariable final int accountNumber)

	{
		return "admin eXTRA Account Number" + accountNumber;
	}

	// ---------------------------------------------------------------

	

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String helloForm()

	{
		return "helloForm";
	}

	@RequestMapping(value = "/hello", method = RequestMethod.POST)
	// public String helloForms()
	public String helloForm(HttpServletRequest request, Model model) {
		String name = request.getParameter("name");

		if (name == null || name == "") {
			name = "helloworld";
		}
		Product product = new Product(0L, "Arul", "Android", "Developer");

		productDao.save(product);
		model.addAttribute("name", name);
		model.addAttribute("message", "New product");

		return "void";
	}

}
*/