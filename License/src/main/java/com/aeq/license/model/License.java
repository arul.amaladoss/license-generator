package com.aeq.license.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity(name = "tbl_license")
public class License implements Serializable 
{

	private static final long serialVersionUID = -8254623703272352325L;

	/*
	 * @NotNull private 
	 * Long productKey;
	 */

	@NotNull
	private String holder;

	@NotNull
	private String issuer;

	@NotNull
	private Date issueDate;

	@NotNull
	private Date goodAfterDate;

	@NotNull
	private Date goodBeforeDate;

	private Long noOfLicenses = new Long(0);

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Version
	@Column(name = "version")
	private Integer version;

	@Column(name = "username", nullable = false)
	private String username = "";

	@Column(name = "password", nullable = false)
	private String password = "";

	@Column(name = "email", nullable = false)
	private String email = "";

	@OneToMany(mappedBy = "license", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<LicenseProduct> productBeans = new ArrayList<LicenseProduct>();

	public License() 
	{

	}

	public Long getId() 
	{
		return this.id;
	}

	public void setId(final Long id) 
	{
		this.id = id;
	}

	public Integer getVersion() 
	{
		return this.version;
	}

	public void setVersion(final Integer version) 
	{
		this.version = version;
	}

	/*
	 * public Long getProductKey() { return this.productKey; }
	 * 
	 * public void setProductKey(final Long productKey) { this.productKey =
	 * productKey; }
	 */

	public String getHolder() 
	{
		return this.holder;
	}

	public void setHolder(final String holder) 
	{
		this.holder = holder;
	}

	public String getIssuer() 
	{
		return this.issuer;
	}

	public void setIssuer(final String issuer) 
	{
		this.issuer = issuer;
	}

	public Date getIssueDate() 
	{
		return this.issueDate;
	}

	public void setIssueDate(final Date issueDate) 
	{
		this.issueDate = issueDate;
	}

	public Date getGoodAfterDate() 
	{
		return this.goodAfterDate;
	}

	public void setGoodAfterDate(final Date goodAfterDate) 
	{
		this.goodAfterDate = goodAfterDate;
	}

	public Date getGoodBeforeDate() 
	{
			return this.goodBeforeDate;
	}

	public void setGoodBeforeDate(final Date goodBeforeDate) 
	{
		this.goodBeforeDate = goodBeforeDate;
	}

	public void setNoOfLicenses(final Long noOfLicenses) 
	{
		this.noOfLicenses = noOfLicenses;
	}

	public String getUsername() 
	{
		return username;
	}

	public void setUsername(final String username) 
	{
		this.username = username;
	}

	public String getPassword() 
	{
		return password;
	}

	public void setPassword(final String password) 
	{
		this.password = password;
	}

	public String getEmail() 
	{
		return email;
	}

	public void setEmail(final String email) 
	{
		this.email = email;
	}

	public List<LicenseProduct> getProductBeans() 
	{
		return productBeans;
	}

	public void setProductBeans(List<LicenseProduct> productBeans) 
	{
		this.productBeans = productBeans;
	}

	public Long getNoOfLicenses() 
	{
		return noOfLicenses;
	}

	@Override
	public String toString() 
	{
		return "License [ holder=" + holder + ", issuer=" + issuer + ", issueDate=" + issueDate + ", goodAfterDate="
				+ goodAfterDate + ", goodBeforeDate=" + goodBeforeDate + ", noOfLicenses=" + noOfLicenses + ", id=" + id
				+ ", version=" + version + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", productBeans=" + "]";
	}
}
