package com.aeq.license.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_license_product")
public class LicenseProduct 
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private Product product;

	private Integer value;

	@ManyToOne(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	@JoinColumn(name = "license_id", referencedColumnName = "id")
	private License license;

	public Long getId() 
	{
		return id;
	}

	public void setId(final Long id) 
	{
		this.id = id;
	}

	public Product getProduct() 
	{
		return product;
	}

	public void setProduct(final Product product) 
	{
		this.product = product;
	}

	public Integer getValue() 
	{
		return value;
	}

	public void setValue(final Integer value) 
	{
		this.value = value;
	}

	public License getLicense() 
	{
		return license;
	}

	public void setLicense(final License license) 
	{
		this.license = license;
	}

	@Override
	public String toString() 
	{
		return "LicenseProduct [id=" + id + ", product=" + product + ", value=" + value + ", license=" + license + "]";
	}

}
