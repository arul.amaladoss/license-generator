package com.aeq.license.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Product entity.
 */
@Entity
@Table(name = "tbl_product")
public class Product 
{

	// Fields

	private Long id;
	private String name;
	private String description;
	private String platform;
	// Integer value;

	transient private LicenseProduct productBean;

	// Constructors

	/** default constructor */
	public Product() 
	{
	}

	public Product(final Long id) 
	{
		this.id = id;
	}

	/** minimal constructor */
	public Product(final String name, final String description, final String platform) 
	{
		this.name = name;
		this.description = description;
		this.platform = platform;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() 
	{
		return this.id;
	}

	public void setId(final Long id) 
	{
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 80)
	public String getName() 
	{
		return this.name;
	}

	public void setName(final String name) 
	{
		this.name = name;
	}

	@Column(name = "description", nullable = false, length = 300)
	public String getDescription() 
	{
		return this.description;
	}

	public void setDescription(final String description) 
	{
		this.description = description;
	}

	/*
	 * @Transient public Integer getValue() { return value; }
	 * 
	 * public void setValue(final Integer value) { this.value = value; }
	 */

	public String getPlatform() 
	{
		return platform;
	}

	public void setPlatform(final String platform) 
	{
		this.platform = platform;
	}

	@Transient
	public LicenseProduct getProductBean(final License license) 
	{
		if (productBean == null) 
		{
			productBean = new LicenseProduct();
			productBean.setLicense(license);
			productBean.setProduct(this);
			// productBean.setValue(0);
		}
		return productBean;
	}

}
