package com.aeq.license.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aeq.license.model.License;

@Transactional
@Repository
public interface LicenseDao extends JpaRepository<License, Long> 
{

	public List<License> findAll();

	public License findById(Long lid);

}
