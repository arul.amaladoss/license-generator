package com.aeq.license.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aeq.license.model.Product;

@Transactional
@Repository
public interface ProductDao extends JpaRepository<Product, Long>
{
	
	
	public List<Product> findAll();
	
	public Product findById(Long pid);
	

}
