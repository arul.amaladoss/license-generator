package com.aeq.license.utility;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public  class DatesFormatsChange 
{
	public static java.sql.Date getcurrent() 
	{
		java.sql.Date sql = null;

		try 
		{
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
			String data = df.format(new Date());

			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			Date parsed = format.parse(data);
			sql = new java.sql.Date(parsed.getTime());

		}
		catch (ParseException e) 
		{
			System.out.println(e);
			e.printStackTrace();
		}

	return sql;
	}
}
