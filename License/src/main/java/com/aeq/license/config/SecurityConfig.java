package com.aeq.license.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter 
{

	private static final String USER = "USER";
	private static final String ADMIN = "ADMIN";

	@Autowired
	DataSource dataSource;

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception 
	{
		auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery("select username,password, enabled from users where username=?")
				.authoritiesByUsernameQuery("select username, role from user_roles where username=?");
	}

	@Override
	public void configure(HttpSecurity httpSecurity) throws Exception 
	{
		httpSecurity.csrf().disable().authorizeRequests()

				.antMatchers("/*").hasRole(USER)
			/*	.antMatchers("//*").hasRole(ADMIN)
				.antMatchers("/pcreate/*").hasRole(USER)
				.antMatchers("/plist/*").hasRole(USER)
				.antMatchers("/delete/*").hasRole(USER)
				.antMatchers("/update/*").hasRole(USER)
				.antMatchers("/lcreate/*").hasRole(USER)
				.antMatchers("/llist/*").hasRole(USER)*/
			.and()
				.formLogin();
	}

	
	 /* @Autowired public void configureGlobal(AuthenticationManagerBuilder auth)
	  throws Exception { auth.inMemoryAuthentication()
	  .withUser("Arul").password("User").roles(USER) .and()
	  .withUser("Admin").password("Admin").roles(USER,ADMIN);
	  
	  }*/
	 

}
