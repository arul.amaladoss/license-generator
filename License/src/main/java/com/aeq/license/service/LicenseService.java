package com.aeq.license.service;



import java.util.List;

import com.aeq.license.model.License;



public interface LicenseService {

	public  License save(License license);
	
	public List<License> findAll();
	
	public  void delete(Long id);
	
	public License findOne(Long id);
	
	public Boolean exists(Long id);
}
