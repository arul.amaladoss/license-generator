

package com.aeq.license.service;



import java.util.List;

import com.aeq.license.model.Product;



public interface ProductService 
{

	public  Product save(Product product);
	
	public List<Product> findAll();
	
	public  void delete(Long id);
	
	public Product findOne(Long id);
	
	public Boolean exists(Long id);

}
