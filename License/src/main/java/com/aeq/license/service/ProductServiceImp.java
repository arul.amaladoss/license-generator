package com.aeq.license.service;

import com.aeq.license.dao.ProductDao;
import com.aeq.license.model.Product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class ProductServiceImp implements ProductService 
{

	@Autowired
	ProductDao productDao;

	@Override
	public Product save(Product product) 
	{
		Product currentProduct=null;
		
		if (product != null) 
		{
		currentProduct=productDao.save(product);
		} else 
		{
			System.out.println("Can't save null Object");
		}
		
		return currentProduct;		
	}

	@Override
	public List<Product> findAll() 
	{
			return productDao.findAll();
	}

	@Override
	public void delete(Long id) 
	{
		if (id != null) 
		{
			if (productDao.exists(id)) 
			{
				productDao.delete(id);
			}

		}		
	}

	/*@Override
	public Product findOne(Long id) 
	{
			return productDao.findOne(id);
	}*/

	@Override
	public Boolean exists(Long id) 
	{
		return productDao.exists(id);
	}

	@Override
	public Product findOne(Long id) {
		// TODO Auto-generated method stub
		return productDao.getOne(id);
	}

}