package com.aeq.license.service;

import com.aeq.license.dao.LicenseDao;
import com.aeq.license.model.License;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
@Transactional
public class LicenseServiceImp implements LicenseService {

	@Autowired
	LicenseDao licenseDao;

	@Override
	public License save(License license) 
	{
		return licenseDao.save(license);
	}

	@Override
	public List<License> findAll() 
	{
		return licenseDao.findAll();
	}

	@Override
	public void delete(Long id) 
	{
		if (id != null && licenseDao.exists(id)) 
		{
			licenseDao.delete(id);
		}
		 
	}

	/*@Override
	public License findOne(Long id) 
	{
		return licenseDao.findOne(id);
	}*/

	@Override
	public Boolean exists(Long id) 
	{
		return licenseDao.exists(id);
	}

	@Override
	public License findOne(Long id) {
		// TODO Auto-generated method stub
		return licenseDao.getOne(id);
	}

	

	/*@Override
	public void deleteLicense(final License license) {
		daoLicense.delete(license);
	}

	@Override
	public License findLicense(final Long id) {
		return daoLicense.findOne(id);
	}

	@Override
	public List<License> findAllLicenses() {
		List<License> licenses = new ArrayList<>();
		licenses = daoLicense.findAll();
		// if (licenses != null && licenses.size() > 0) {
		// for (License license : licenses) {
		// license.setGoodAfterDate(LicenseUtil.formateDate(license.getGoodAfterDate()));
		// license.st
		// }
		// }
		return licenses;
	}

	@Override
	public List<License> findLicenseEntries(final int firstResult,
			final int maxResults) {
		return daoLicense.findAll(
				new org.springframework.data.domain.PageRequest(firstResult
						/ maxResults, maxResults)).getContent();
	}

	@Override
	public void saveLicense(final License license) {
		license.setIssueDate(new Date());
		daoLicense.save(license);
	}

	@Override
	public License updateLicense(final License license) {
		return daoLicense.save(license);
	}

	@Override
	public List<License> findLicenseEntries(int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<License> findByProduct(Product product) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<License> findByProduct(final Product product) {
		return daoLicense.findByProductBeans_Product(product);
	}
*/
}

