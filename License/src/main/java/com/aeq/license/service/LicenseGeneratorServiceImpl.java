/*package com.aeq.license.service;



import org.springframework.stereotype.Service;

import com.aeq.license.encription.LicenseCreatorProperties;
import com.aeq.license.model.License;
import com.aeq.license.provider.MCPasswordProvider;
import com.aeq.license.provider.MCPrivateKeyProvider;



@Service("licenseGeneratorService")
public class LicenseGeneratorServiceImpl implements LicenseGeneratorService {

	@Override
	public byte[] createLicense(final License license) {
		try {
			LicenseCreatorProperties
					.setPrivateKeyDataProvider(new MCPrivateKeyProvider());
			LicenseCreatorProperties
					.setPrivateKeyPasswordProvider(new MCPasswordProvider());
			final Builder builder = new Builder();
			if (license.getProductKey() != null
					&& license.getProductKey().trim().length() > 0) {
				builder.withProductKey(license.getProductKey());
			}
			if (license.getHolder() != null
					&& license.getHolder().trim().length() > 0) {
				builder.withHolder(license.getHolder());
			}
			if (license.getGoodAfterDate() != null) {
				builder.withGoodAfterDate(license.getGoodAfterDate().getTime());
			}
			if (license.getGoodBeforeDate() != null) {
				builder.withGoodBeforeDate(license.getGoodBeforeDate()
						.getTime());
			}
			if (license.getIssueDate() != null) {
				builder.withIssueDate(license.getIssueDate().getTime());
			}

			if (license.getIssuer() != null
					&& license.getIssuer().trim().length() > 0) {
				builder.withIssuer(license.getIssuer());
			}
			if (license.getNoOfLicenses() != null
					&& license.getNoOfLicenses() > 0) {
				builder.withNumberOfLicenses(license.getNoOfLicenses()
						.intValue());
			} else {
				builder.withNumberOfLicenses(0);
			}
			if (license.getEmail() != null
					&& license.getEmail().trim().length() > 0) {
				builder.addAdditional("EMAIL", license.getEmail());
			}
			if (license.getEmail() != null
					&& license.getEmail().trim().length() > 0) {
				builder.addAdditional("USERNAME", license.getUsername());
			}
			if (license.getEmail() != null
					&& license.getEmail().trim().length() > 0) {
				builder.addAdditional("PASSWORD", license.getPassword());
			}

			if (license.getProductBeans() != null
					&& license.getProductBeans().size() > 0) {
				for (final LicenseProduct productBean : license
						.getProductBeans()) {
					if (productBean.getValue() != null
							&& productBean.getValue() > 0) {
						// builder.addAdditional("PRODUCT_" +
						// LicenseUtil.toJSONString(productBean.getProduct()),
						// productBean.getValue());
						final Product product = productBean.getProduct();
						builder.addAdditional(
								"PRODUCT_" + product.getName() + "_"
										+ product.getPlatform() + "_"
										+ product.getDescription(),
								productBean.getValue());
					}
				}
			}
			final net.nicholaswilliams.java.licensing.License genereatedLicense = builder
					.build();
			return LicenseCreator.getInstance().signAndSerializeLicense(
					genereatedLicense);
		} catch (final Throwable ex) {

		}
		return null;
	}
}
*/