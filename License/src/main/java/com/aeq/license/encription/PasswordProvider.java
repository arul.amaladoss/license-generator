package com.aeq.license.encription;

public interface PasswordProvider
{
	public char[] getPassword();
}
