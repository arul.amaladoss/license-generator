package com.aeq.license.encription;

import com.aeq.license.exception.KeyNotFoundException;

public interface PrivateKeyDataProvider
{
	public byte[] getEncryptedPrivateKeyData() throws KeyNotFoundException;
}
