package com.aeq.license.provider;

import com.aeq.license.encription.PasswordProvider;

public final class MCPasswordProvider implements PasswordProvider 
{
	@Override
	public char[] getPassword() 
	{
		return new char[] 
				{
				0x00000033, 0x00000030,
				0x00000042,	0x00000031,
				0x00000037, 0x00000033,
				0x00000020, 0x00000063,
				0x00000052,	0x00000079,
				0x00000050, 0x00000074,
				0x00000030 
				
				};
	}
}
